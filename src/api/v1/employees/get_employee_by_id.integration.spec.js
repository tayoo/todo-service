const supertest = require("supertest")
const make_app = require("../../../make_app")
const {TESTING_EMPLOYEES_IDs} = require("../../../constants")
const {get_employee_by_id} = require("../../../services/employees")

let app
beforeEach(async () => {
	app = await make_app()
})

describe("Test the GET /employees/{id} endpoint", () => {
	test("A DEVELOPER can't retrieve someone else's account", async () => {
		const res = await supertest(app)
			.get("/v1/employees/1")
			.set("user_id", TESTING_EMPLOYEES_IDs.DEVELOPER) //this will create a header
		expect(res.statusCode).toEqual(403)
		expect(res.body).toEqual({error: "Your role doesn't allow you to do this operation"})
	})
	test("A DEVELOPER can retrieve his account", async () => {
		const res = await supertest(app)
			.get("/v1/employees/" + TESTING_EMPLOYEES_IDs.DEVELOPER)
			.set("user_id", TESTING_EMPLOYEES_IDs.DEVELOPER) //this will create a header
		expect(res.statusCode).toEqual(200)
	})
	test("A PROJECT-MANAGER can retrieve any account but without score and salary", async () => {
		const res = await supertest(app)
			.get("/v1/employees/3")
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(200)
		expect(res.body.score).toEqual(undefined)
		expect(res.body.salary).toEqual(undefined)
	})
	test("An HR can retrieve any account fully", async () => {
		const res = await supertest(app)
			.get("/v1/employees/" + TESTING_EMPLOYEES_IDs.HR)
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(200)
		const employee = await get_employee_by_id(TESTING_EMPLOYEES_IDs.HR)
		expect(res.body).toEqual(employee)
	})
})
