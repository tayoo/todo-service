const supertest = require("supertest")
const make_app = require("../../../make_app")
const {TESTING_EMPLOYEES_IDs} = require("../../../constants")
let app
beforeEach(async () => {
	app = await make_app()
})

describe("Test the PATCH /employees/id endpoint: check roles access", () => {
	test("A DEVELOPER can't update scores", async () => {
		const res = await supertest(app)
			.patch("/v1/employees/1")
			.set("user_id", TESTING_EMPLOYEES_IDs.DEVELOPER) //this will create a header
		expect(res.statusCode).toEqual(403)
		expect(res.body).toEqual({error: "Your role doesn't allow you to do this operation"})
	})
	test("A PROJECT_MANAGER can't update scores", async () => {
		const res = await supertest(app)
			.patch("/v1/employees/2")
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(403)
		expect(res.body).toEqual({error: "Your role doesn't allow you to do this operation"})
	})
})

describe("Test the PATCH /employees/id endpoint: check body validation", () => {
	test("Missing body", async () => {
		const res = await supertest(app)
			.patch("/v1/employees/4")
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({
			error: "Bad request: You should enter a valid score between 0 and 100",
		})
	})
	test("Invalid body: missing score", async () => {
		const res = await supertest(app)
			.patch("/v1/employees/5")
			.send({dummy: "dummy"}) //body
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({
			error: "Bad request: You should enter a valid score between 0 and 100",
		})
	})
	test("Invalid body: non numeric score", async () => {
		const res = await supertest(app)
			.patch("/v1/employees/5")
			.send({score: "abc"}) //body
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({
			error: "Bad request: You should enter a valid score between 0 and 100",
		})
	})
	test("Invalid body: score out of [0..100] range", async () => {
		const res = await supertest(app)
			.patch("/v1/employees/5")
			.send({score: 101}) //body
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({
			error: "Bad request: You should enter a valid score between 0 and 100",
		})
	})
	test("An HR can update the score with value in [0..100] range", async () => {
		const res = await supertest(app)
			.patch("/v1/employees/" + TESTING_EMPLOYEES_IDs.HR)
			.send({score: 50}) //body
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(200)
		expect(res.body.score).toEqual(50)
	})
})
