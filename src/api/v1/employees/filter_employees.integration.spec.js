const supertest = require("supertest")
const make_app = require("../../../make_app")
const {ROLES, TESTING_EMPLOYEES_IDs} = require("../../../constants")

let app
beforeEach(async () => {
	app = await make_app()
})

describe("Test the GET /employees endpoint: check roles access", () => {
	test("A DEVELOPER can't filter accounts", async () => {
		const res = await supertest(app)
			.get("/v1/employees")
			.set("user_id", TESTING_EMPLOYEES_IDs.DEVELOPER) //this will create a header
		expect(res.statusCode).toEqual(403)
		expect(res.body).toEqual({error: "Your role doesn't allow you to do this operation"})
	})
	test("A PROJECT-MANAGER can retrieve any account but without score and salary", async () => {
		const res = await supertest(app)
			.get("/v1/employees")
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(200)
		expect(res.body.score).toEqual(undefined)
		expect(res.body.salary).toEqual(undefined)
	})
	test("An HR can retrieve any account fully", async () => {
		const res = await supertest(app).get("/v1/employees").set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(200)
	})
})

describe("Test the GET /employees endpoint: check filters", () => {
	test("Get Employees with name containing Someone", async () => {
		const res = await supertest(app)
			.get("/v1/employees?name=Someone")
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(200)
		expect(res.body.every((x) => x.name.includes("Someone"))).toEqual(true)
	})
	test("Get Employees by project_id = project-2", async () => {
		const res = await supertest(app)
			.get("/v1/employees?project_id=project-2")
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(200)
		expect(res.body.every((x) => x.projects.includes("project-2"))).toEqual(true)
	})
	test("Get Employees by role = HR", async () => {
		const res = await supertest(app)
			.get("/v1/employees?role=HR")
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(200)
		expect(res.body.every((x) => x.role === "HR")).toEqual(true)
	})
	test("Test combining more than one filter", async () => {
		const res = await supertest(app)
			.get("/v1/employees?project_id=project-2&name=Someone")
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(200)
		expect(res.body.every((x) => x.projects.includes("project-2"))).toEqual(true)
		expect(res.body.every((x) => x.name.includes("Someone"))).toEqual(true)
	})
})
