const {ROLES} = require("../../../constants")
const filter_employee_by_id = require("../../../services/employees").get_employee_by_id

const get_employee_by_id = async (req, res) => {
	let employee = await filter_employee_by_id(req.params.id)
	if (employee && req.user_role === ROLES.PROJECT_MANAGER) {
		delete employee.score
		delete employee.salary
	}
	res.status(200).send(employee)
}
module.exports = get_employee_by_id
