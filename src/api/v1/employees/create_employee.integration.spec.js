const supertest = require("supertest")
const make_app = require("../../../make_app")
const {TESTING_EMPLOYEES_IDs} = require("../../../constants")
let app
beforeEach(async () => {
	app = await make_app()
})

const valid_body = {
	name: "SomeOne",
	email: "someone@someone.com",
	team: "A",
	role: "HR",
}

describe("Test the PUT /employees endpoint: check roles access", () => {
	test("A DEVELOPER can't create an employee", async () => {
		const res = await supertest(app)
			.put("/v1/employees")
			.set("user_id", TESTING_EMPLOYEES_IDs.DEVELOPER) //this will create a header
		expect(res.statusCode).toEqual(403)
		expect(res.body).toEqual({error: "Your role doesn't allow you to do this operation"})
	})
	test("A PROJECT_MANAGER can't create an employee", async () => {
		const res = await supertest(app)
			.put("/v1/employees")
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(403)
		expect(res.body).toEqual({error: "Your role doesn't allow you to do this operation"})
	})
	test("A HR can create an employee", async () => {
		const res = await supertest(app)
			.put("/v1/employees")
			.send(valid_body) //body
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(200)
		expect(typeof res.body.id !== undefined).toEqual(true)
	})
})

describe("Test the PUT /employees endpoint: check body validation", () => {
	test("Missing body", async () => {
		const res = await supertest(app).put("/v1/employees").set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({error: "Bad request: Missing body"})
	})
	test("Invalid body: missing name", async () => {
		let body = {...valid_body}
		delete body.name
		const res = await supertest(app)
			.put("/v1/employees")
			.send(body) //body
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({error: "Bad request: Invalid fields name"})
	})
	test("Invalid body: missing email", async () => {
		let body = {...valid_body}
		delete body.email
		const res = await supertest(app)
			.put("/v1/employees")
			.send(body) //body
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({error: "Bad request: Invalid fields email"})
	})
	test("Invalid body: invalid email", async () => {
		const res = await supertest(app)
			.put("/v1/employees")
			.send({...valid_body, email: "someone"}) //body
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({error: "Bad request: Invalid fields email"})
	})
	test("Invalid body: missing team", async () => {
		let body = {...valid_body}
		delete body.team
		const res = await supertest(app)
			.put("/v1/employees")
			.send(body) //body
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({error: "Bad request: Invalid fields team"})
	})
	test("Invalid body: missing role", async () => {
		let body = {...valid_body}
		delete body.role
		const res = await supertest(app)
			.put("/v1/employees")
			.send(body) //body
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({error: "Bad request: Invalid fields role"})
	})
	test("Invalid body: invalid role", async () => {
		const res = await supertest(app)
			.put("/v1/employees")
			.send({...valid_body, role: "Dummy"}) //body
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({error: "Bad request: Invalid fields role"})
	})
	test("Invalid body: more than one fields are invalid", async () => {
		let body = {...valid_body}
		delete body.name
		const res = await supertest(app)
			.put("/v1/employees")
			.send({...body, role: "Dummy"}) //body
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({error: "Bad request: Invalid fields name, role"})
	})
})
