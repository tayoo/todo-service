const {get_employee_by_id} = require("../../../services/employees")
const update_employee_score = require("../../../services/employees").evaluate_employee

const evaluate_employee = async (req, res) => {
	if (!validate_score(req.body)) {
		res.status(400).send({
			error: "Bad request: You should enter a valid score between 0 and 100",
		})
		return
	}
	await update_employee_score(req.params.id, req.body.score)
	const employee = await get_employee_by_id(req.params.id)
	res.status(200).send(employee)
}

const validate_score = (body) => {
	return body && body.score && !isNaN(body.score) && body.score >= 0 && body.score <= 100
}
module.exports = evaluate_employee
