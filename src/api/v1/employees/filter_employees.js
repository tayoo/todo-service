const {ROLES} = require("../../../constants")
const {get_all_employees} = require("../../../services/employees")

const filter_employees = async (req, res) => {
	let employees = await get_all_employees()
	if (req.query.name) employees = filter_employees_by_name(employees, req.query.name)
	if (req.query.role) employees = filter_employees_by_role(employees, req.query.role)
	if (req.query.project_id)
		employees = filter_employees_by_project_id(employees, req.query.project_id)
	if (req.user_role === ROLES.PROJECT_MANAGER) {
		employees = employees.map((employee) => {
			delete employee.score
			delete employee.salary
			return employee
		})
	}
	res.status(200).send(employees)
}

const filter_employees_by_name = (array, name) => {
	return array.filter((employee) => {
		employee.name.includes(name)
	})
}

const filter_employees_by_role = (array, role) => {
	return array.filter((employee) => {
		employee.role === role
	})
}

const filter_employees_by_project_id = (array, project_id) => {
	return array.filter((employee) => {
		employee.projects.includes(project_id)
	})
}

module.exports = filter_employees
