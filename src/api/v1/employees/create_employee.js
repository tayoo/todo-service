const {ROLES} = require("../../../constants")
const {validate_string_field, validate_email} = require("../../../utils/request_validator")
const {add_employee} = require("../../../services/employees")

const create_employee = async (req, res) => {
	if (!req.body || Object.keys(req.body).length === 0) {
		res.status(400).send({error: "Bad request: Missing body"})
		return
	}
	const required_fields = ["name", "email", "team", "role"]
	const invalid_fields = validate_create_employee_body(req.body, required_fields)
	if (invalid_fields && invalid_fields.length > 0) {
		res.status(400).send({error: `Bad request: Invalid fields ${invalid_fields.join(", ")}`})
		return
	}
	const employee_id = await add_employee(req.body)
	res.status(200).send({
		id: employee_id,
	})
}

const validate_create_employee_body = (body, required_fields) => {
	let invalid_fields = []
	required_fields.forEach((required_field) => {
		if (!validate_string_field(body[required_field])) invalid_fields.push(required_field)
		else if (required_field === "email" && !validate_email(body[required_field]))
			invalid_fields.push(required_field)
		else if (
			required_field === "role" &&
			![ROLES.DEVELOPER, ROLES.PROJECT_MANAGER, ROLES.HR].includes(body[required_field])
		)
			invalid_fields.push(required_field)
	})
	return invalid_fields
}

module.exports = create_employee
