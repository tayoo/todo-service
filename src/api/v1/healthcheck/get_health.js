const {performance} = require("perf_hooks")
const axios = require("axios")
const API = axios.create({
	baseURL: process.env.PING_URL,
})

API.interceptors.request.use(
	(config) => {
		const newConfig = {...config}
		newConfig.metadata = {startTime: new Date()}
		return newConfig
	},
	(error) => {
		return Promise.reject(error)
	}
)
API.interceptors.response.use(
	(response) => {
		const newRes = {...response}
		newRes.config.metadata.endTime = new Date()
		newRes.duration = newRes.config.metadata.endTime - newRes.config.metadata.startTime
		return newRes
	},
	(error) => {
		const newError = {...error}
		newError.config.metadata.endTime = new Date()
		newError.duration = newError.config.metadata.endTime - newError.config.metadata.startTime
		return Promise.reject(newError)
	}
)

const get_health = (req, res) => {
	const start = performance.now()
	const promises = []
	const healthCheck = {
		durationInMillis: 0,
		tests: [],
	}
	promises.push(buildPromise(process.env.PING_URL, healthCheck))
	Promise.all(promises)
		.then(() => {
			healthCheck.durationInMillis = Math.floor(performance.now() - start)
			res
				.status(healthCheck.tests.find((x) => x.testResult === "FAILED") ? 503 : 200)
				.send(healthCheck)
		})
		.catch((error) => {
			console.log(
				`Unexpected error while performing health check. Err: ${error.message}, Stack: ${error.stack}`
			)
		})
}

const buildPromise = (endpoint, healthCheck) => {
	let healthCheckTest = {}
	return new Promise(async (resolve, reject) => {
		await API.get(endpoint)
			.then((response) => {
				healthCheckTest = {
					testName: endpoint,
					durationInMillis: response && response.duration ? response.duration : 0,
					testResult: response.status === 200 || response.status === 204 ? "PASSED" : "FAILED",
				}
			})
			.catch((error) => {
				healthCheckTest = {
					testName: endpoint,
					durationInMillis: error.duration,
					testResult: "FAILED",
					error: error.message,
				}
			})
		healthCheck.tests.push(healthCheckTest)
		resolve()
	})
}
module.exports = get_health
