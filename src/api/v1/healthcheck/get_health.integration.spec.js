const supertest = require("supertest")
const make_app = require("../../../make_app")
let app
beforeEach(async () => {
	app = await make_app()
})

describe("Test the healthcheck module", () => {
	test("The healthcheck should succeed when pinging a valid host", async () => {
		process.env.PING_URL = "https://www.google.com/"
		const res = await supertest(app).get("/v1/healthcheck")
		expect(res.statusCode).toEqual(200)
		expect(
			res.body.tests.filter((x) => x.testName === "https://www.google.com/")[0].testResult
		).toEqual("PASSED")
	})
	test("The healthcheck should fail when pinging an invalid host", async () => {
		process.env.PING_URL = "dummy-testing-fake.url.test"
		const res = await supertest(app).get("/v1/healthcheck")
		expect(res.statusCode).toEqual(503)
		expect(
			res.body.tests.filter((x) => x.testName === "dummy-testing-fake.url.test")[0].testResult
		).toEqual("FAILED")
	})
})
