const supertest = require("supertest")
const make_app = require("../../../make_app")
const {TESTING_EMPLOYEES_IDs} = require("../../../constants")
const {get_all_projects} = require("../../../services/projects")
const {get_employee_by_id} = require("../../../services/employees")

let app
beforeEach(async () => {
	app = await make_app()
})

describe("Test the GET /projects endpoint", () => {
	test("A PROJECT-MANAGER can retrieve all projects", async () => {
		const res = await supertest(app)
			.get("/v1/projects")
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(200)
		const projects = await get_all_projects()
		expect(res.body).toEqual(projects)
	})
	test("An HR can't retrieve any project", async () => {
		const res = await supertest(app).get("/v1/projects").set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(403)
		expect(res.body).toEqual({error: "Your role doesn't allow you to do this operation"})
	})
	test("A DEVELOPER can retrieve only the projects he's assigned to", async () => {
		const res = await supertest(app)
			.get("/v1/projects")
			.set("user_id", TESTING_EMPLOYEES_IDs.DEVELOPER) //this will create a header
		expect(res.statusCode).toEqual(200)
		const employee = await get_employee_by_id(TESTING_EMPLOYEES_IDs.DEVELOPER)
		expect(res.body.every((p) => employee.projects.includes(p.id))).toEqual(true)
	})
})
