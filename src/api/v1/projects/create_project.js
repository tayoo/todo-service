const {add_project} = require("../../../services/projects")
const {validate_string_field, validate_url} = require("../../../utils/request_validator")

const create_project = async (req, res) => {
	if (!req.body || Object.keys(req.body).length === 0) {
		res.status(400).send({error: "Bad request: Missing body"})
		return
	}
	if (!validate_string_field(req.body.name)) {
		res.status(400).send({error: `Bad request: Invalid field name`})
		return
	}
	if (validate_string_field(req.body.url) && !validate_url(req.body.url)) {
		res.status(400).send({error: `Bad request: Invalid field url`})
		return
	}
	const project_id = await add_project(req.body)
	res.status(200).send({
		id: project_id,
	})
}

module.exports = create_project
