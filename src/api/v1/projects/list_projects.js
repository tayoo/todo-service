const {ROLES} = require("../../../constants")
const {get_all_projects} = require("../../../services/projects")
const {get_employee_by_id} = require("../../../services/employees")

const list_projects = async (req, res) => {
	let projects = await get_all_projects()
	if (req.user_role === ROLES.DEVELOPER) {
		const employee = await get_employee_by_id(req.headers["user_id"])
		if (projects && employee)
			projects = projects.filter((project) => {
				if (employee.projects.includes(project.id)) return project
			})
		else projects = []
	}
	res.status(200).send(projects)
}
module.exports = list_projects
