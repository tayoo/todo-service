const supertest = require("supertest")
const make_app = require("../../../make_app")
const {TESTING_EMPLOYEES_IDs} = require("../../../constants")

let app
beforeEach(async () => {
	app = await make_app()
})

describe("Test the PUT /projects endpoint: check permissions", () => {
	test("A PROJECT-MANAGER can create projects", async () => {
		const res = await supertest(app)
			.put("/v1/projects")
			.send({name: "project-10"})
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(200)
		expect(typeof res.body.id !== undefined).toEqual(true)
	})
	test("An HR can't create projects", async () => {
		const res = await supertest(app).put("/v1/projects").set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(403)
		expect(res.body).toEqual({error: "Your role doesn't allow you to do this operation"})
	})
	test("A DEVELOPER can't create projects'", async () => {
		const res = await supertest(app)
			.put("/v1/projects")
			.set("user_id", TESTING_EMPLOYEES_IDs.DEVELOPER) //this will create a header
		expect(res.statusCode).toEqual(403)
		expect(res.body).toEqual({error: "Your role doesn't allow you to do this operation"})
	})
})

describe("Test the PUT /projects endpoint: body validation", () => {
	test("Invalid body: missing name should be rejected", async () => {
		const res = await supertest(app)
			.put("/v1/projects")
			.send({dummy: "project-10"})
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({error: `Bad request: Invalid field name`})
	})
	test("Invalid body: invalid url should be rejected", async () => {
		const res = await supertest(app)
			.put("/v1/projects")
			.send({name: "project-10", url: "dummy"})
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({error: `Bad request: Invalid field url`})
	})
})
