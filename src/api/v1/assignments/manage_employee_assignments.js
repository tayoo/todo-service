const {validate_string_field, validate_boolean} = require("../../../utils/request_validator")
const {update_employee_assignments} = require("../../../services/employees")

const manage_employee_assignments = async (req, res) => {
	if (!req.body || Object.keys(req.body).length === 0) {
		res.status(400).send({error: "Bad request: Missing body"})
		return
	}
	const required_fields = ["employee_id", "project_id", "assign"]
	const invalid_fields = validate_assignment_body(req.body, required_fields)
	if (invalid_fields && invalid_fields.length > 0) {
		res.status(400).send({error: `Bad request: Invalid fields ${invalid_fields.join(", ")}`})
		return
	}
	const employee = await update_employee_assignments(req.body)
	res.status(200).send(employee)
}

const validate_assignment_body = (body, required_fields) => {
	let invalid_fields = []
	required_fields.forEach((required_field) => {
		if (required_field !== "assign" && !validate_string_field(body[required_field]))
			invalid_fields.push(required_field)
		else if (required_field === "assign" && !validate_boolean(body[required_field]))
			invalid_fields.push(required_field)
	})
	return invalid_fields
}

module.exports = manage_employee_assignments
