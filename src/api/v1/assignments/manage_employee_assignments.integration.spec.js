const supertest = require("supertest")
const make_app = require("../../../make_app")
const {TESTING_EMPLOYEES_IDs} = require("../../../constants")

let app
beforeEach(async () => {
	app = await make_app()
})

describe("Test the POST /assignments endpoint", () => {
	test("A PROJECT-MANAGER can create projects", async () => {
		const res = await supertest(app)
			.post("/v1/assignments")
			.send({
				employee_id: TESTING_EMPLOYEES_IDs.PROJECT_MANAGER,
				project_id: "project-1",
				assign: true,
			})
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(200)
		expect(res.body.projects.includes("project-1")).toEqual(true)
	})
	test("An HR can't assign / unassign employees to projects", async () => {
		const res = await supertest(app)
			.post("/v1/assignments")
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(403)
		expect(res.body).toEqual({error: "Your role doesn't allow you to do this operation"})
	})
	test("A DEVELOPER can't assign / unassign employees to projects", async () => {
		const res = await supertest(app)
			.post("/v1/assignments")
			.set("user_id", TESTING_EMPLOYEES_IDs.DEVELOPER) //this will create a header
		expect(res.statusCode).toEqual(403)
		expect(res.body).toEqual({error: "Your role doesn't allow you to do this operation"})
	})
	test("Invalid body: missing body, should be rejected", async () => {
		const res = await supertest(app)
			.post("/v1/assignments")
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({error: "Bad request: Missing body"})
	})
	test("Invalid body: missing employee_id, should be rejected", async () => {
		const res = await supertest(app)
			.post("/v1/assignments")
			.send({
				project_id: "project-1",
				assign: true,
			})
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({error: `Bad request: Invalid fields employee_id`})
	})
	test("Invalid body: missing employee_id, should be rejected", async () => {
		const res = await supertest(app)
			.post("/v1/assignments")
			.send({
				employee_id: TESTING_EMPLOYEES_IDs.PROJECT_MANAGER,
				assign: true,
			})
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({error: `Bad request: Invalid fields project_id`})
	})
	test("Invalid body: missing assign, should be rejected", async () => {
		const res = await supertest(app)
			.post("/v1/assignments")
			.send({
				employee_id: TESTING_EMPLOYEES_IDs.PROJECT_MANAGER,
				project_id: "project-1",
			})
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({error: `Bad request: Invalid fields assign`})
	})
	test("Invalid body: missing more than one field, should be rejected", async () => {
		const res = await supertest(app)
			.post("/v1/assignments")
			.send({
				assign: true,
			})
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(400)
		expect(res.body).toEqual({error: `Bad request: Invalid fields employee_id, project_id`})
	})
})
