const filter_employees = require("./v1/employees/filter_employees")
const create_employee = require("./v1/employees/create_employee")
const get_employee_by_id = require("./v1/employees/get_employee_by_id")
const evaluate_employee = require("./v1/employees/evaluate_employee")
const list_projects = require("./v1/projects/list_projects")
const create_project = require("./v1/projects/create_project")
const manage_employee_assignments = require("./v1/assignments/manage_employee_assignments")
const get_health = require("./v1/healthcheck/get_health")

module.exports = {
	filter_employees,
	create_employee,
	get_employee_by_id,
	evaluate_employee,
	list_projects,
	create_project,
	manage_employee_assignments,
	get_health,
}
