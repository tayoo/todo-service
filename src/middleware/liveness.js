const express = require("express")

const liveness = express.Router().get("/", (req, res) => {
	res.status(200).send({success: true})
})

module.exports = liveness
