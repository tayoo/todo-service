const rateLimit = require("express-rate-limit")

const rate_limiter = rateLimit({
	windowMs: process.env.REQUEST_VELOCITY_WINDOW || 60000,
	max: process.env.REQUEST_VELOCITY_LIMIT || 1000,
	handler: (req, res) => {
		console.error(
			`Request velocity exceeded (${req.rateLimit.current} out of ${req.rateLimit.limit} max requests) ${req.ip} - ${req.url}`
		)
		res.status(429).send("Request velocity exceeded")
	},
})

module.exports = rate_limiter
