const supertest = require("supertest")
const make_app = require("../make_app")
let app
beforeEach(async () => {
	app = await make_app()
})

describe("Test the liveness module", () => {
	test("The / endpoint should be successful", async () => {
		const res = await supertest(app).get("/")
		expect(res.statusCode).toEqual(200)
		expect(res.body).toEqual({success: true})
	})
})
