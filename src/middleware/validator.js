const {validationResult, check} = require("express-validator")

const validate = (req, res, next) => {
	check(req.query, "Query Validation Failed!")
	check(req.params, "Params Validation Failed!")
	check(req.body, "Body Validation Failed!")
	const errors = validationResult(req)
	if (errors.isEmpty()) {
		return next()
	}
	const extractedErrors = []
	errors.array().map((err) => extractedErrors.push({[err.param]: err.msg}))

	return res.status(422).json({
		errors: extractedErrors,
	})
}

module.exports = validate
