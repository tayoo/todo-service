const supertest = require("supertest")
const make_app = require("../make_app")
let app
beforeEach(async () => {
	app = await make_app()
})

describe("Test the auth module", () => {
	test("The healthcheck endpoint shouldn't require auth", async () => {
		process.env.PING_URL = "https://www.google.com/"
		const res = await supertest(app).get("/v1/healthcheck")
		expect(res.statusCode).toEqual(200)
	})
	test("Missing user_id header should be rejected", async () => {
		const res = await supertest(app).get("/v1/employees/1")
		expect(res.statusCode).toEqual(401)
		expect(res.body).toEqual({error: "Unauthorized"})
	})
	test("A user_id with invalid role should be rejected", async () => {
		const res = await supertest(app).get("/v1/employees/1").set("user_id", "dummy-123") //this will create a header
		expect(res.statusCode).toEqual(403)
		expect(res.body).toEqual({error: "Your role doesn't allow you to do this operation"})
	})
})
