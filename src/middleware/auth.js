const {ROLES} = require("../constants")
const {get_employee_by_id} = require("../services/employees")
Cache = require("cache")
cache = new Cache()

const auth = async (req, res, next) => {
	if (req.path === "/v1/healthcheck") {
		next()
		return
	}
	try {
		const user_id = req.headers["user_id"]
		if (!user_id)
			throw {
				status: 401,
				message: "Unauthorized",
			}
		const user_role = await get_user_role(user_id)
		req.user_role = user_role
		const InsufficientPermissionError = {
			status: 403,
			message: "Your role doesn't allow you to do this operation",
		}
		switch (user_role) {
			case ROLES.DEVELOPER:
				if (req.path.includes(`projects`) && req.method === "GET") break
				if (!req.path.includes(`employees/${user_id}`) || req.method !== "GET")
					throw InsufficientPermissionError
				break
			case ROLES.PROJECT_MANAGER:
				if (req.path.includes("employees") && req.method !== "GET")
					throw InsufficientPermissionError
				break
			case ROLES.HR:
				if (req.path.includes("assignments") || req.path.includes("projects"))
					throw InsufficientPermissionError
				break
			default:
				throw InsufficientPermissionError
		}
		next()
	} catch (error) {
		console.log("error : " + JSON.stringify(error))
		res.status(error.status).send({
			error: error.message,
		})
		return
	}
}

const get_user_role = async (user_id) => {
	const key = `user_role-${user_id}`
	let user_role = cache.get(key)
	if (!user_role) {
		const employee = await get_employee_by_id(user_id)
		if (employee) user_role = employee.role
		cache.put(key, user_role)
	}
	return user_role
}

module.exports = auth
