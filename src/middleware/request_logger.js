const logger = (req, res, next) => {
	console.log(`${req.method} ${req.path}`) //this can be using any logging module
	next()
}

module.exports = logger
