const express = require("express")
const SwaggerParser = require("swagger-parser")
const {connector} = require("swagger-routes-express")
const api = require("./api")
const load_middleware = require("./utils/load_middleware")
const middleware = require("./middleware")
const OpenApiValidator = require("express-openapi-validator")
const error_handler = require("./utils/error_handler")
const make_app = async () => {
	const parser = new SwaggerParser()
	const api_definition_location = "./src/api/api.yaml"
	const api_description = await parser.validate(api_definition_location)
	const connect = connector(api, api_description)
	const app = express()
	load_middleware(app, middleware)
	connect(app)
	error_handler(app)
	app.use(
		OpenApiValidator.middleware({
			apiSpec: api_definition_location,
			validateResponses: false,
			validateRequests: true,
		})
	)
	return app
}

module.exports = make_app
