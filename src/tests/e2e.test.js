const supertest = require("supertest")
const make_app = require("../make_app")
const {TESTING_EMPLOYEES_IDs} = require("../constants")

beforeEach(async () => {
	app = await make_app()
})

const valid_body = {
	name: "e2e",
	email: "e2e@someone.com",
	team: "A",
	role: "HR",
	score: 1,
}
let new_employee_id = ""
let new_project_id = ""
let initial_employees_count = 0
let initial_projects_count = 0

describe("e2e: Test the entire rest API", () => {
	test("Get the old count of employees", async () => {
		const res = await supertest(app).get("/v1/employees").set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(200)
		initial_employees_count = res.body.length
	})
	test("Create a new employee", async () => {
		const res = await supertest(app)
			.put("/v1/employees")
			.send(valid_body) //body
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(200)
		new_employee_id = res.body.id
		expect(typeof res.body.id !== undefined).toEqual(true)
	})
	test("Double check that the count of employees increased by 1", async () => {
		const res = await supertest(app).get("/v1/employees").set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(200)
		expect(res.body.length).toEqual(initial_employees_count + 1)
	})
	test("Make sure the new employee exists and can be fetched", async () => {
		const res = await supertest(app)
			.get(`/v1/employees/${new_employee_id}`)
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(200)
		expect(res.body.id).toEqual(new_employee_id)
	})
	test("Update the score of the new employee", async () => {
		const res = await supertest(app)
			.patch(`/v1/employees/${new_employee_id}`)
			.send({score: 75}) //body
			.set("user_id", TESTING_EMPLOYEES_IDs.HR) //this will create a header
		expect(res.statusCode).toEqual(200)
		expect(res.body.score).toEqual(75)
	})
	test("Get the initial list of projects", async () => {
		const res = await supertest(app)
			.get("/v1/projects")
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(200)
		initial_projects_count = res.body.length
	})
	test("Create a new project", async () => {
		const res = await supertest(app)
			.put("/v1/projects")
			.send({name: "project-e2e"})
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(200)
		new_project_id = res.body.id
		expect(typeof res.body.id !== undefined).toEqual(true)
	})
	test("Make sure the count of projects is increased by 1", async () => {
		const res = await supertest(app)
			.get("/v1/projects")
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(200)
		expect(res.body.length).toEqual(initial_projects_count + 1)
	})
	test("Assign the new employee to the new project", async () => {
		const res = await supertest(app)
			.post("/v1/assignments")
			.send({
				employee_id: new_employee_id,
				project_id: new_project_id,
				assign: true,
			})
			.set("user_id", TESTING_EMPLOYEES_IDs.PROJECT_MANAGER) //this will create a header
		expect(res.statusCode).toEqual(200)
		expect(res.body.projects.includes(new_project_id)).toEqual(true)
	})
})
