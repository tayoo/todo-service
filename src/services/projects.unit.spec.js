const {get_all_projects, add_project} = require("./projects")

describe("Test the projects service", () => {
	test("test get_all_projects: invalid path", async () => {
		const projects = await get_all_projects("weirdpath")
		expect(projects).toEqual([])
	})
	test("test get_all_projects; valid path", async () => {
		const projects = await get_all_projects()
		expect(projects.length > 0).toEqual(true)
	})
	test("test add_project: missing data", async () => {
		const id = await add_project()
		expect(id).toEqual(undefined)
	})
	test("test add_project: valid", async () => {
		const id = await add_project({name: "unit-test-project"})
		expect(typeof id).toEqual("string")
	})
})
