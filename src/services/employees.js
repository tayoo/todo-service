const jsonfile = require("jsonfile")
const file = "./src/services/employees.json"
const uuidv4 = require("uuid").v4

const get_all_employees = async (path) => {
	return await jsonfile
		.readFile(path || file)
		.then((obj) => {
			return obj
		})
		.catch((error) => {
			console.log(error)
			return []
		})
}

const get_employee_by_id = async (id) => {
	try {
		const employees = await get_all_employees()
		if (!employees) throw "database error"
		return employees.find((e) => e.id === id)
	} catch {
		return undefined
	}
}

const add_employee = async (employee) => {
	try {
		if (!employee) throw "missing employee data"
		let employees = await get_all_employees()
		const id = uuidv4()
		employees.push({...employee, id: id, projects: []})
		return jsonfile.writeFile(file, employees).then((res) => {
			return id
		})
	} catch (error) {
		console.log(error)
		return undefined
	}
}

const evaluate_employee = async (id, score) => {
	let employees = await get_all_employees()
	const index = employees.findIndex((e) => e.id === id)
	if (index > -1) {
		employees.splice(index, 1, {...employees[index], score: score})
		await jsonfile
			.writeFile(file, employees)
			.then((res) => {})
			.catch((error) => console.error(error))
		return true
	} else {
		return false
	}
}

const update_employee_assignments = async ({employee_id, project_id, assign}) => {
	let employees = await get_all_employees()
	const index = employees.findIndex((e) => e.id === employee_id)
	if (index > -1) {
		let new_projects = employees[index].projects
		let project_index = new_projects.findIndex((p) => p.id === project_id)
		if (assign) {
			if (project_index === -1) new_projects.push(project_id)
		} else {
			if (project_index > -1) new_projects.splice(project_index, 1)
		}
		employees.splice(index, 1, {...employees[index], projects: [...new_projects]})
		await jsonfile
			.writeFile(file, employees)
			.then((res) => {})
			.catch((error) => console.error(error))
		return employees[index]
	} else {
		return false
	}
}

module.exports = {
	get_all_employees,
	get_employee_by_id,
	add_employee,
	evaluate_employee,
	update_employee_assignments,
}
