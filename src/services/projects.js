const jsonfile = require("jsonfile")
const file = "./src/services/projects.json"
const uuidv4 = require("uuid").v4

const get_all_projects = async (path) => {
	return await jsonfile
		.readFile(path || file)
		.then((obj) => {
			return obj
		})
		.catch((error) => {
			console.log(error)
			return []
		})
}

const add_project = async (project) => {
	try {
		if (!project) throw "missing project data"
		let projects = await get_all_projects()
		const id = uuidv4()
		projects.push({...project, id: id})
		return jsonfile.writeFile(file, projects).then((res) => {
			return id
		})
	} catch (error) {
		console.log(error)
		return undefined
	}
}
module.exports = {get_all_projects, add_project}
