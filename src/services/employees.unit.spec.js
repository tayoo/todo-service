const {
	get_all_employees,
	get_employee_by_id,
	add_employee,
	evaluate_employee,
	update_employee_assignments,
} = require("./employees")
let all_employees = []
let new_employee_id = ""
describe("Test the employees service", () => {
	test("test get_all_employees: invalid path", async () => {
		const employees = await get_all_employees("weirdpath")
		expect(employees).toEqual([])
	})
	test("test get_all_employees; valid path", async () => {
		const employees = await get_all_employees()
		expect(employees.length > 0).toEqual(true)
		all_employees = employees
	})
	test("test get_employee_by_id: invalid id", async () => {
		const employee = await get_employee_by_id("yoyo")
		expect(employee).toEqual(undefined)
	})
	test("test get_employee_by_id: valid id", async () => {
		const employee = await get_employee_by_id(all_employees[0].id)
		expect(employee.id).toEqual(all_employees[0].id)
	})
	test("test add_employee: missing data", async () => {
		const id = await add_employee()
		expect(id).toEqual(undefined)
	})
	test("test add_employee: valid", async () => {
		const id = await add_employee({name: "unit-test-employee"})
		expect(typeof id).toEqual("string")
		new_employee_id = id
	})
	test("test evaluate_employee: invalid id", async () => {
		const result = await evaluate_employee("yoyo", 10)
		expect(result).toEqual(false)
	})
	test("test evaluate_employee: valid", async () => {
		const result = await evaluate_employee(new_employee_id, 10)
		expect(result).toEqual(true)
	})
	test("test update_employee_assignments: invalid id", async () => {
		const result = await update_employee_assignments({
			employee_id: "yoyo",
			project_id: "abc",
			assign: true,
		})
		expect(result).toEqual(false)
	})
	test("test update_employee_assignments: valid", async () => {
		const result = await update_employee_assignments({
			employee_id: new_employee_id,
			project_id: "project-2",
			assign: true,
		})
		expect(result.projects.includes("project-2")).toEqual(true)
	})
})
