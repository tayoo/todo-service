const validate_string_field = (input) => {
	if (!input || typeof input !== "string" || input.trim().length <= 0) {
		return false
	}
	return true
}

const validate_email = (input) => {
	return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(input)
}

const validate_url = (input) => {
	try {
		new URL(input)
		return true
	} catch (err) {
		return false
	}
}

const validate_boolean = (input) => {
	return typeof input === "boolean"
}

module.exports = {validate_string_field, validate_email, validate_url, validate_boolean}
