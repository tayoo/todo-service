const load_middleware = (app, middleware) => {
	for (const middle of Object.keys(middleware)) {
		app.use(middleware[middle])
	}
}

module.exports = load_middleware
