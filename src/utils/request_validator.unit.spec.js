const {
	validate_string_field,
	validate_email,
	validate_url,
	validate_boolean,
} = require("./request_validator")

describe("Test the request_validator util", () => {
	test("test validate_string_field", () => {
		expect(validate_string_field(undefined)).toEqual(false)
		expect(validate_string_field(" ")).toEqual(false)
		expect(validate_string_field(2)).toEqual(false)
		expect(validate_string_field("abc")).toEqual(true)
	})
	test("test validate_email", () => {
		expect(validate_email(undefined)).toEqual(false)
		expect(validate_email("someone.com")).toEqual(false)
		expect(validate_email("someone@some.com")).toEqual(true)
	})
	test("test validate_url", () => {
		expect(validate_url(undefined)).toEqual(false)
		expect(validate_url("something")).toEqual(false)
		expect(validate_url("someone.com")).toEqual(false)
		expect(validate_url("https://someone.com")).toEqual(true)
	})
	test("test validate_boolean", () => {
		expect(validate_boolean(undefined)).toEqual(false)
		expect(validate_boolean("abc")).toEqual(false)
		expect(validate_boolean(20)).toEqual(false)
		expect(validate_boolean(true)).toEqual(true)
		expect(validate_boolean(false)).toEqual(true)
	})
})
