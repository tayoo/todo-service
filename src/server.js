const make_app = require("./make_app")

make_app()
	.then((app) => app.listen(process.env.MICROSERVICE_PORT))
	.then(() => console.log("Server started " + process.env.MICROSERVICE_PORT))
	.catch((err) => console.error("Fatal server error", err))
