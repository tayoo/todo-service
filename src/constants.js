const ROLES = {
	DEVELOPER: "DEVELOPER",
	PROJECT_MANAGER: "PROJECT-MANAGER",
	HR: "HR",
}
//the following variables are some employee ids with a specific role for testing purposes
const TESTING_EMPLOYEES_IDs = {
	DEVELOPER: "the-developer",
	PROJECT_MANAGER: "the-project-manager",
	HR: "the-hr",
}
module.exports = {
	ROLES,
	TESTING_EMPLOYEES_IDs,
}
