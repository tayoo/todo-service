Thank you for dedicating your time to check this work :)
I don't have hands on experience using Nest JS so I created this Rest API using express and openApi. That's being said, I'm totally open to work with Nest JS in the future.

# Getting Started
- git clone git@gitlab.com:tayoo/rest-api.git
- cd rest-api
- npm install
- create a `.env` file in the root level, for local testing you can set the following content

`MICROSERVICE_PORT=5000` // this is the port of the application

`REQUEST_VELOCITY_LIMIT=1000` // this is a throttling limit

`PING_URL=https://www.google.com/` // this is the URL that the healthcheck endpoint will ping. In general in this endpoint we ping the API URLs that we're consuming in this project to make sure they are healthy.

- npm run dev
- /docs endpoint returns the Swagger documentation of the API.

**Testing**

Essentially, the test pyramid describes that you should write unit tests, integration tests and end-to-end tests as well. You should have more integration tests than end-to-end tests, and even more unit tests. Let's take a look at how you can add unit tests for your applications!

To run all the tests of this project (execpt the e2e, as the tests run in parallel so to avoid conflicts in the json file), do `npm run test`

- In this project, the unit tests live next to the original script. For example the unit test file of get_employee.js is get_employee.spec.js in th same project hierarchy.

To run unit tests, do `npm run unit-tests`
- Integration tests are grouped by domain. So in total we have 3 that are split accordingly:
1 for employees
1 for projects
1 for assignments

To run integration tests, do `npm run integration-tests`
- End to end testing is to make sure that the entire api is linked to each other. So scenarios here are cross domains.

To run e2e tests, do `npm run e2e-tests`

# Architecture
- Make the server.js the simplest possible
- Having the make_app function, it loads all the middlewares at once.
- Any functionality we want it to be applied to the API globally, can be exposed under src/middleware
- The index under /middleware is to export the entire module at once
- no explicit routing files, using the connector provided by swagger-routes-express.
  That maps endpoint/controller by the operationId. That's why every operationId has a file under /api/v1 with the same name. This makes the project more lightweight and organized.

# Security
- The majority of the security approach is applicable on all the endpoints, so making this a middleware willbe applied for all endpoints, corner cases only are handeled in specific endpoints.
- That's why adding a new param in the req object called req.user_role
- The healthcheck endpoint should be public, because it will be used by other systems that don't need to authorize like normal users. For example when deploying it behind a load balancer and the need to perform recurring healthchecks.
- Caching the map between user_id and user_role to improve performance, as this data is used for every single request and the role of an employee is not designed to change frequently.
- Doing try catch, to send the errored responses all together, in the catch block with dynamic status and message.
